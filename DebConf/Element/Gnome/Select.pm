#!/usr/bin/perl -w

=head1 NAME

Debconf::Element::Gnome::Select - drop down select box widget

=cut

package Debconf::Element::Gnome::Select;
use strict;
use Gtk;
use Gnome;
use Debconf::Element::Gnome; # perlbug
use base qw(Debconf::Element::Select Debconf::Element::Gnome);

=head1 DESCRIPTION

This is a drop down select box widget.

=cut

=head1 METHODS

=over 4

=cut

sub get_history {
    my $this=shift;
    my $menu = $this->get_menu;
    my @children = $menu->children;
    my $item = $menu->get_active;
    my $i;

    for ($i=0; $i <= $#children; $i++) {
	if ($children[$i] eq $item) {
	    last;
	}
    }

    return $i;
}

sub init {
	my $this=shift;

	my $menu = new Gtk::Menu;
	$menu->show;
	my $previous = undef;
	my $menu_item;
	my $history = 0;

	my $default=$this->translate_default;
	my @choices=$this->{question}->choices_split;

	$this->{widget} = new Gtk::OptionMenu;
	$this->{widget}->show;

	#$this->{widget}->set_popdown_strings(@choices);
	my $option_menu = new Gtk::Menu;
	my $option_menu_item;
	my $choice;

	foreach $choice (@choices) {
	  print "The choice is $choice\n";
	  $option_menu_item = new Gtk::MenuItem ($choice);
	  $option_menu->append ($option_menu_item);
	  $option_menu_item->show();
	}
	$this->{widget}->set_menu ($option_menu);
	$option_menu->show;

	#$this->{widget}->set_value_in_list(1, 0);
	#$this->{widget}->entry->set_editable(0);

	#if (defined($default) and length($default) != 0) {
	#    $this->{widget}->entry->set_text($default);
	#} else {
	#    $this->{widget}->entry->set_text($choices[0]);
	#}
}

=item value

The value is just the value field of the widget, translated back to the C
locale.

=cut

sub value {
	my $this=shift;
	my @choices=$this->{question}->choices_split;
	my $some_widget;
	my $some_index;
	$some_widget = $this->{widget}->get_menu()->get_active();
	#$some_index = g_list_index ($this->{widget}->get_menu()->children, $some_widget);
	$some_index = $this->{widget}->get_menu()->children->index($some_widget);
	print ("Index was $some_index\n");
	return choices[$some_index];
}

=back

=head1 AUTHOR

Joey Hess <joey@kitenet.net>

=cut

1
