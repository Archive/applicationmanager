class StatusInformation;

#ifndef STATUS_INFORMATION_HH
#define STATUS_INFORMATION_HH

#include <sys/types.h>

#include <libgnomeuimm.h>

#include <libgnomeuimm/appbar.h>

#include <gtkmm/button.h>
#include <gtkmm.h>
#include <string>

#include "Operation.hh"

class StatusInformation : public SigC::Object
{
public: 
  StatusInformation (Gnome::UI::AppBar *bar);

  void UpdateCurrent (int current_bytes, int current_items);
  void UpdateTotal (int total_bytes, int total_items);
  void SetPercent (float percentage);

  void UpdateDescription (string status);

  void NotifyOperationComplete (Operation *operation);

  void SetObject (string object);

private:
  Gnome::UI::AppBar *bar;

  Operation *operation;

  string object;

  bool pending_call;
  bool operation_complete;

  float percentage;
  int current_bytes;
  int current_items;
  int total_bytes;
  int total_items;
  string status;

  SigC::Connection connection;
  GMutex *connection_mutex;

  bool UpdateInsideGTKLoop (void);
  void RegisterForUpdate (void);

};

#endif
