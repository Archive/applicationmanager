#ifndef INSTALL_SEARCH_HH
#define INSTALL_SEARCH_HH

/*
#include <gnome--/dialog.h>
*/

#include <libgnomeuimm.h>

#include <gtkmm.h>

#include <glade/glade.h>

#include "PackageBackend.hh"

class InstallSearch : public SigC::Object
{
public:
  InstallSearch (const char *glade_filename, PackageBackend *package_backend);
private:

  Gtk::Dialog *InstallDialogue;
  Gtk::Entry *SearchPattern;
  Gtk::Button *InstallButton;
  //Gtk::CList *MatchList;

  GladeXML *window_xml;

  PackageBackend *package_backend;

  void InstallButtonClicked (void);
  void SearchPatternChanged (void);
};

#endif
