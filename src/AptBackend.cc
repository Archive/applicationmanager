extern "C" {
#include <sys/types.h>
#include <regex.h>
}

#include "AptBackend.hh"
#include "AptStatus.hh"
#include "AptIncludes.hh"
#include "UpdateOperation.hh"
#include "PackageInformation.hh"

#include <apt-pkg/pkgcache.h>
#include <apt-pkg/pkgcachegen.h>

using std::cout;
using std::string;

#include "AptCacheFile.hh"

static void check_errors (string tag)
{
  while (_error->PendingError()) {
    string message;
    _error->PopMessage (message);
    cout << tag << message << "\n";
  }
}

bool InstallPackages (AptCacheFile &Cache);

AptBackend::AptBackend (StatusInformation *status)
{ 
  pkgInitConfig (*_config);
  pkgInitSystem (*_config, _system);
  
  this->status = status;
  cache_uninitialized = true;
}

OperationResult AptBackend::InitializeDatabase (void)
{
  check_errors ("Somebody Else's Error ");

  if (cache_uninitialized) {
    //cout << "Opening cache...\n";
    //Cache.Close();
    if (Cache.Open(status, false) == false) {
      //cout << "Unable to open package database\n";
      return ERROR_NO_PERMISSION;
    }
    if (Cache.CheckDeps() == false) {
      cout << "Dependency check failed. Please try running 'apt-get -f install' from a command prompt.\n";
    }
    
    //cout << "Broken count is: " << Cache->BrokenCount() << "\n";
    
    //cout << "Install count is: " << Cache->InstCount() << "\n";
    
    pkgProblemResolver Fix (Cache);
    cache_uninitialized = false;
  }
  while (_error->PendingError()) {
    string message;
    _error->PopMessage (message);
    cout << "Initialize Error: " << message << "\n";
  }
  return NO_ERROR;
}

void AptBackend::perform_operation (Operation *operation)
{
  operation->GetPackageList()->SetupCache (Cache);
  Print();
  //Lock();
  InstallPackages (Cache);
  Cache.Close();
  cache_uninitialized = true;
  operation->Complete (NO_ERROR);
}

void AptBackend::Lock (void)
{
  check_errors ("Before lock errors:");
  _system->Lock();
  check_errors ("Due to lock errors:");
}

void AptBackend::Unlock (void)
{
  check_errors ("Before unlock errors:");
  _system->UnLock();
  check_errors ("Due to unlock errors:");
}

void AptBackend::perform_update (Operation *operation)
{
  OperationResult result;

  status->UpdateDescription ("Downloading the list of available packages...");

  //Lock();

  // Download the new set of packages from the Debian servers
  result = UpdatePackageList ();
  if (result != NO_ERROR) {
    operation->Complete (result);
    return;
  }

  // Update the database to reflect these changes
  result = InitializeDatabase (); 
  if (result != NO_ERROR) {
    operation->Complete (result);
    return;
  }

  cout << "Sending operating complete for update from backend\n";
  //status->OperationComplete (NO_ERROR);
  operation->Complete (NO_ERROR);
}

UpdateOperation *AptBackend::update (void)
{
  return new UpdateOperation (this, status);
}

Operation *AptBackend::upgrade (void)
{
  // Do the upgrade
  if (pkgAllUpgrade(Cache) == false) {
    //ShowBroken(c1out,Cache,false);
    cout << _error->Error("Internal Error, AllUpgrade broke stuff");
  }
   
  return new Operation (UPGRADE_OPERATION, this, status, new PackageList (Cache));
}

Operation *AptBackend::install (string name)
{
  pkgCache::PkgIterator pkg;

  status->SetObject (name);

  OperationResult result = InitializeDatabase ();
  if (result != NO_ERROR) {
    return NULL;
  }

  Cache.EmptyChanges();

  pkg = Cache->FindPkg(name);

  Cache->MarkInstall(pkg,true);

  return new Operation (INSTALL_OPERATION, this, status, new PackageList (Cache));
}

void AptBackend::Print ()
{
  unsigned int j;
  int keep = 0;
  int install = 0;
  int remove = 0;
  int upgrade = 0;

  for (j = 0; j < Cache->Head().PackageCount; j++) {
    pkgCache::PkgIterator *I = new pkgCache::PkgIterator(Cache,Cache.List[j]);
    if (Cache[*I].Keep() == true) {
      keep++;
    } else if (Cache[*I].Delete() == true) {
      remove++;
    } else if (Cache[*I].NewInstall() == true) {
      install++;
    } else if (Cache[*I].Upgrade() == true) {
      upgrade++;
    }
  }

  cout << "Unchanged: " << keep << " | To Install: " << install << " | To Remove: " << remove << " | To Upgrade: " << upgrade << endl;
}

Operation *AptBackend::remove (string name)
{
  pkgCache::PkgIterator pkg;

  status->SetObject (name);

  OperationResult result = InitializeDatabase ();
  if (result != NO_ERROR) {
    return NULL;
  }

  Cache.EmptyChanges();

  pkg = Cache->FindPkg (name);
  Cache->MarkDelete (pkg,true);

  return new Operation (REMOVE_OPERATION, this, status, new PackageList (Cache));
}

bool AptBackend::InstallPackages(AptCacheFile &Cache)
{
   // Create the text record parser
   pkgRecords Recs(Cache);
   if (_error->PendingError() == true)
      return false;
   
   // Lock the archive directory
   FileFd Lock;
   Lock.Fd(GetLock(_config->FindDir("Dir::Cache::Archives") + "lock"));

   if (_error->PendingError() == true) {
     cout << "Unable to lock the download directory";
   }
   
   // Create the download object
   unsigned int width = 100;
   AptStatus Stat (status);
   pkgAcquire Fetcher(&Stat);

   // Read the source list
   pkgSourceList List;
   if (List.ReadMainList() == false) {
     cout << "The list of sources could not be read.";
   }
   
   // Create the package manager and prepare to download
   SPtr<pkgPackageManager> PM= _system->CreatePM(Cache);
   if (PM->GetArchives(&Fetcher,&List,&Recs) == false || 
       _error->PendingError() == true)
      return false;

   // Run it
   while (1)
   {
      bool Transient = false;
      if (_config->FindB("APT::Get::Download",true) == false)
      {
	 for (pkgAcquire::ItemIterator I = Fetcher.ItemsBegin(); I < Fetcher.ItemsEnd();)
	 {
	    if ((*I)->Local == true)
	    {
	       I++;
	       continue;
	    }

	    // Close the item and check if it was found in cache
	    (*I)->Finished();
	    if ((*I)->Complete == false)
	       Transient = true;
	    
	    // Clear it out of the fetch list
	    delete *I;
	    I = Fetcher.ItemsBegin();
	 }	 
      }
      
      if (Fetcher.Run() == pkgAcquire::Failed)
	 return false;
      
      // Print out errors
      bool Failed = false;
      for (pkgAcquire::ItemIterator I = Fetcher.ItemsBegin(); I != Fetcher.ItemsEnd(); I++)
      {
	 if ((*I)->Status == pkgAcquire::Item::StatDone &&
	     (*I)->Complete == true)
	    continue;
	 
	 if ((*I)->Status == pkgAcquire::Item::StatIdle)
	 {
	    Transient = true;
	    // Failed = true;
	    continue;
	 }

	 //fprintf(stderr,_("Failed to fetch %s  %s\n"),(*I)->DescURI().c_str(),
	 //	 (*I)->ErrorText.c_str());
	 Failed = true;
      }

      
      this->Unlock ();
      pkgPackageManager::OrderResult Res = PM->DoInstall();
      if (Res == pkgPackageManager::Failed || _error->PendingError() == true)
	 return false;
      if (Res == pkgPackageManager::Completed)
	 return true;
      
      // Reload the fetcher object and loop again for media swapping
      Fetcher.Shutdown();
      if (PM->GetArchives(&Fetcher,&List,&Recs) == false)
	 return false;
      
      this->Lock();
   }   
}

OperationResult AptBackend::UpdatePackageList (void)
{
  OperationResult result;

  // Get the source list
  pkgSourceList List;
  if (List.ReadMainList() == false)
    return ERROR_NO_PERMISSION;
  
  // Lock the list directory
  FileFd Lock;
  if (_config->FindB("Debug::NoLocking",false) == false)
    {
      Lock.Fd(GetLock(_config->FindDir("Dir::State::Lists") + "lock"));
      if (_error->PendingError() == true)
	cout << _error->Error("Unable to lock the list directory");
    }
   
  // Create the download object
  AptStatus Stat (status);
  pkgAcquire Fetcher(&Stat);   

  // Populate it with the source selection
  if (List.GetIndexes(&Fetcher) == false)
    return ERROR_NO_PERMISSION;
   
  // Run it
  if (Fetcher.Run() == pkgAcquire::Failed)
    return ERROR_NO_PERMISSION;

  bool Failed = false;
  for (pkgAcquire::ItemIterator I = Fetcher.ItemsBegin(); I != Fetcher.ItemsEnd(); I++)
    {
      if ((*I)->Status == pkgAcquire::Item::StatDone)
	continue;

      (*I)->Finished();
      
      fprintf(stderr, "Failed to fetch %s  %s\n",(*I)->DescURI().c_str(),
	      (*I)->ErrorText.c_str());
      Failed = true;
    }
   
  // Clean out any old list files
  if (_config->FindB("APT::Get::List-Cleanup",true) == true)
    {
      if (Fetcher.Clean(_config->FindDir("Dir::State::lists")) == false ||
	  Fetcher.Clean(_config->FindDir("Dir::State::lists") + "partial/") == false)
	return ERROR_NO_PERMISSION;
    }
   
  if (Failed == true)
    cout << _error->Error("Some index files failed to download, they have been ignored, or old ones used instead.");
   
  return NO_ERROR;
}

// LocalitySort - Sort a version list by package file locality		/*{{{*/
// ---------------------------------------------------------------------
/* */
static int 
LocalityCompare(const void *a, const void *b)
{
   pkgCache::VerFile *A = *(pkgCache::VerFile **)a;
   pkgCache::VerFile *B = *(pkgCache::VerFile **)b;
   
   if (A == 0 && B == 0)
      return 0;
   if (A == 0)
      return 1;
   if (B == 0)
      return -1;
   
   if (A->File == B->File)
      return A->Offset - B->Offset;
   return A->File - B->File;
}

static void 
LocalitySort(pkgCache::VerFile **begin,
	     unsigned long Count,size_t Size)
{   
   qsort(begin,Count,Size,LocalityCompare);
}

GList *AptBackend::search (string search_pattern)
{
  struct ExVerFile
  {
    pkgCache::VerFile *Vf;
    bool NameMatch;
  };

  MMap *Map;
  bool NamesOnly = false;
  pkgDepCache::Policy Plcy;
   
  regex_t Pattern;

  GList *list = NULL;
  
  SrcList = new pkgSourceList;
  SrcList->ReadMainList();

  OpProgress opp;

  pkgMakeStatusCache(*SrcList,opp,&Map,true);

  pkgCache Cache(Map);
   

  if (regcomp(&Pattern, search_pattern.c_str(), REG_EXTENDED | REG_ICASE | 
	      REG_NOSUB) != 0) {
    regfree(&Pattern);
    return NULL; // regex compilation error
  }      
   
  // Create the text record parser
  pkgRecords Recs(Cache);

  check_errors ("Pre-Search Error: ");
   
  ExVerFile *VFList = new ExVerFile[Cache.HeaderP->PackageCount+1];
  memset(VFList,0,sizeof(*VFList)*Cache.HeaderP->PackageCount+1);

  // Map versions that we want to write out onto the VerList array.
  for (pkgCache::PkgIterator P = Cache.PkgBegin(); P.end() == false; P++) {
    VFList[P->ID].NameMatch = true;
    if (regexec(&Pattern, P.Name(),0,0,0) == 0) {
      VFList[P->ID].NameMatch &= true;
    } else {
      VFList[P->ID].NameMatch = false;
    }

     
    // Doing names only, drop any that dont match..
    if (NamesOnly == true && VFList[P->ID].NameMatch == false)
      continue;
	 
    // Find the proper version to use. 
    pkgCache::VerIterator V = Plcy.GetCandidateVer(P);
    if (V.end() == false)
      VFList[P->ID].Vf = V.FileList();
  }
      
  // Include all the packages that provide matching names too
  for (pkgCache::PkgIterator P = Cache.PkgBegin(); P.end() == false; P++) {
    if (VFList[P->ID].NameMatch == false)
      continue;
       
    for (pkgCache::PrvIterator Prv = P.ProvidesList() ; Prv.end() == false; Prv++) {
      pkgCache::VerIterator V = Plcy.GetCandidateVer(Prv.OwnerPkg());
      if (V.end() == false) {
	VFList[Prv.OwnerPkg()->ID].Vf = V.FileList();
	VFList[Prv.OwnerPkg()->ID].NameMatch = true;
      }
    }
  }

  LocalitySort(&VFList->Vf,Cache.HeaderP->PackageCount,sizeof(*VFList));

  // Iterate over all the version records and check them
  for (ExVerFile *J = VFList; J->Vf != 0; J++) {
    pkgRecords::Parser &P = Recs.Lookup(pkgCache::VerFileIterator(Cache,J->Vf));
     
    bool Match = true;
    if (J->NameMatch == false) {
      string LongDesc = P.LongDesc();
      Match = true;
      if (regexec(&Pattern,LongDesc.c_str(),0,0,0) == 0)
	Match &= true;
      else
	Match = false;
    }
     
    if (Match == true) {
      PackageInformation *info = new PackageInformation (P.Name(), P.ShortDesc());
      list = g_list_append (list, info);
      //printf("%s - %s\n",P.Name().c_str(),P.ShortDesc().c_str());
    }
  }
   
  delete [] VFList;
  regfree(&Pattern);

  return list;
}
