#include <libgnomeuimm.h>

#include <glade/glade.h>
#include <libgnomeui/gnome-window-icon.h>

#include "ApplicationManager.hh"

extern "C" {
#include "ensure-root.h"
}

#include "config.h"


using std::cout;

int main (int argc, char *argv[])
{
  string icon_filename;
  ApplicationManager *manager;

  Gnome::Main main ("ApplicationManager", VERSION, 
		    Gnome::UI::module_info_get(),
		    argc, argv,
		    NULL, 0, NULL);


  glade_init();
  /*g_thread_init(NULL);*/

  ensure_root (argv);

  gnome_window_icon_set_default_from_file (PIXMAPDIR "application-manager2.xpm");
  gnome_window_icon_init();

  manager = new ApplicationManager (GLADEDIR "/application-manager.glade");

  main.run();

  return 0;
}
