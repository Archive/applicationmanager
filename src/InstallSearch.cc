#include "InstallSearch.hh"
#include "GladeHelper.hh"
#include "PackageInformation.hh"

InstallSearch::InstallSearch (const char *glade_filename, PackageBackend *package_backend)
{
  this->package_backend = package_backend;

  window_xml = glade_xml_new (glade_filename, "InstallDialogue", NULL);

  if (!window_xml) {
    cout << "WARNING: could not load user interface from file " << glade_filename << "." << endl;
  }

  InstallDialogue  =  getWidgetPtr<Gtk::Dialog>(window_xml, "InstallDialogue");
  SearchPattern    =  getWidgetPtr<Gtk::Entry>(window_xml, "SearchPattern");
  InstallButton    =  getWidgetPtr<Gtk::Button>(window_xml, "InstallButton");
  // FIXME GNOME2: MatchList        =  getWidgetPtr<Gtk::CList>(window_xml, "MatchList");

  SearchPattern->signal_changed().connect (slot (*this, &InstallSearch::SearchPatternChanged));
  InstallButton->signal_clicked().connect (slot (*this, &InstallSearch::InstallButtonClicked));

  InstallDialogue->show();
  this->SearchPatternChanged();
}

void InstallSearch::InstallButtonClicked (void)
{
  cout << "I got clicked!" << endl;
}

void InstallSearch::SearchPatternChanged (void)
{
  string search_pattern;
  GList *list, *node;
  const char *text[3];

  /* FIXME GNOME 2
  MatchList->clear();

  search_pattern = SearchPattern->get_text();

  list = package_backend->search (search_pattern);

  for (node = list; node != NULL; node = node->next) {
    PackageInformation *info = (PackageInformation *)node->data;

    text[0] = info->Name.c_str();
    text[1] = info->Description.c_str();
    text[2] = NULL;
    MatchList->append (text);
  }
  */
}
