class PackageInformation;

#ifndef PACKAGE_INFORMATION_HH
#define PACKAGE_INFORMATION_HH

#include <string>

using std::string;

class PackageInformation
{
public:
  PackageInformation (string name, string description) { Name = name; Description = description; };
  string Name;
  string Description;
};

#endif
