#include "ensure-root.h"
#include "xst-su.h"
#include <gnome.h>
#include <unistd.h>
#include <sys/types.h>

static void
authenticate (gchar *exec_path)
{
        GtkWidget *error_dialog;
        gchar *password;
        gint result;

        for (;;)
        {
                result = xst_su_get_password (&password);

                if (result < 0)
                        exit (0);
                else if (result == 0)
                        return;

                /* If successful, the following never returns */

                xst_su_run_with_password (exec_path, password);

                if (strlen (password))
                        memset (password, 0, strlen (password));

                error_dialog = gnome_error_dialog (_("The password you entered is invalid."));
                gnome_dialog_run_and_close (GNOME_DIALOG (error_dialog));
        }
}

static gint
do_auth (gpointer data) {
  authenticate ((char *)data);
  return 0;
}

int
ensure_root (char **argv)
{
  uid_t uid;

  uid = geteuid ();

  if (geteuid () == 0) {
    // we are already root, great
    return TRUE;
  } else {
    authenticate (argv[0]);

    return TRUE;
  }
}
