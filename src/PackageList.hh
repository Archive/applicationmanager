class PackageList;

#ifndef PACKAGE_LIST_HH
#define PACKAGE_LIST_HH

#include <glib.h>
#include <string>

#include "AptCacheFile.hh"

class PackageList {
public:
  GList *newinstalls;
  GList *removes;
  GList *upgrades;

  PackageList (AptCacheFile &Cache);
  void PackageList::SetupCache (AptCacheFile &Cache);

  GList *make_package_info_list (GList *iterator_list);
private:
  AptCacheFile *Cache;
};

#endif
