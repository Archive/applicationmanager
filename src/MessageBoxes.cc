#include "MessageBoxes.hh"

#include <gtkmm.h>

ErrorMessage::ErrorMessage (Glib::ustring message) {
  Gtk::MessageDialog *box;
      
  box = new Gtk::MessageDialog (message, Gtk::MESSAGE_ERROR);
  
  box->show();
  box->run();
}

InformationMessage::InformationMessage (Glib::ustring message) {
  Gtk::MessageDialog *box;
      
  box = new Gtk::MessageDialog (message);
  
  box->show();
  box->run();
}
