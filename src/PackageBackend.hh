class PackageBackend;

#ifndef PACKAGE_BACKEND_HH
#define PACKAGE_BACKEND_HH

#include <string>
#include <pthread.h>

class PackageBackend;

#include "PackageList.hh"
#include "Operation.hh"
#include "UpdateOperation.hh"

class PackageBackend
{
public:
  virtual void Print (void) = 0;

  virtual void perform_operation (Operation *operation) = 0;
  virtual void perform_update    (Operation *operation) = 0;

  virtual GList *search (string name) = 0;
  virtual Operation *install (string name) = 0;
  virtual Operation *remove  (string name) = 0;
  virtual Operation *upgrade (void) = 0;
  virtual UpdateOperation *update  (void) = 0;
 };

void run_perform_update    (PackageBackend *backend, Operation *operation);
void run_perform_operation (PackageBackend *backend, Operation *operation);

#endif
