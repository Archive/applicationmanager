#ifndef APT_OP_PROGRESS_HH
#define APT_OP_PROGRESS_HH

#include "AptIncludes.hh"
#include "StatusInformation.hh"

class AptOpProgress : public OpProgress
{
protected:

  string OldOp;
  virtual void Update();
  StatusInformation *status;

public:

  virtual void Done() { return; };
  
  AptOpProgress (StatusInformation *status) { this->status = status; }
  virtual ~AptOpProgress () { Done(); };
};

#endif
