#include "Operation.hh"
#include "PackageBackend.hh"

Operation::Operation (OperationType operation_type, PackageBackend *package_backend, 
		      StatusInformation *status, PackageList *packages) 
{
  this->packages = packages;
  this->package_backend = package_backend; 
  this->status = status;
  this->type = operation_type;
}

Operation::Operation (OperationType operation_type, PackageBackend *package_backend, 
		      StatusInformation *status)
{ 
   this->package_backend = package_backend; 
   this->status = status;
   this->type = operation_type;
}

void Operation::EmitCompleteSignal (void)
{
  cout << "4b) Emitting complete signal\n";
  sig_complete.emit (this);
}

void Operation::RegisterCallback (SigC::Slot1<void,Operation *> callback)
{
  this->sig_complete.connect (callback);
}

PackageList *Operation::GetPackageList (void)
{
  return packages;
}

void Operation::Perform (void)
{
  run_perform_operation (package_backend, this);
}

void Operation::Complete (OperationResult result)
{
  cout << "1) sending complete signal from " << get_operation_description (type) << "operation\n";
  status->NotifyOperationComplete (this);
}

OperationResult Operation::GetResult (void)
{
  return this->result;
}

string get_operation_description (OperationType op)
{

  if (op == UPGRADE_OPERATION)
    return _("upgrading packages to the latest versions");
  else if (op==UPDATE_OPERATION)
    return _("updating the package list"); // 
  else if (op==INSTALL_OPERATION)
    return _("installing packages");
  else if (op==REMOVE_OPERATION)
    return _("removing packages");
  else
    return _("unknown operation");
}
