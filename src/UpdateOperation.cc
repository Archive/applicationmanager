#include "UpdateOperation.hh"
#include "PackageBackend.hh"

UpdateOperation::UpdateOperation (PackageBackend *backend, StatusInformation *status) : Operation (UPDATE_OPERATION, backend, status)
{
}

PackageList *UpdateOperation::GetPackageList (void)
{
  return NULL;
}

void UpdateOperation::Perform (void)
{
  run_perform_update (package_backend, this);
}

void UpdateOperation::EmitCompleteSignal (void)
{
  cout << "4a) emitting signal from update\n";
  sig_complete.emit (this);
}

void UpdateOperation::RegisterCallback (SigC::Slot1<void,UpdateOperation *> callback)
{
  this->sig_complete.connect (callback);
}
