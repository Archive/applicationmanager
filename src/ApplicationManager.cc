#include <gtkmm.h>
#include <gtkmm/main.h>

#include <sstream>

#include "ApplicationManager.hh"
#include "GladeHelper.hh"
#include "AptBackend.hh"
#include "AptStatus.hh"
#include "MessageBoxes.hh"

using std::cout;

using SigC::bind;
using SigC::slot;

ApplicationManager::ApplicationManager (const char *glade_filename)
{
  Gnome::UI::AppBar *app_bar;
  Gtk::VBox *box;

  //Gtk::Image *pixmap;

  window_xml = glade_xml_new (glade_filename, NULL, NULL);

  this->glade_filename = g_strdup (glade_filename);

  if (!window_xml) {
    cout << "WARNING: could not load user interface from file " << glade_filename << ".\n";
  }

  window         = getWidgetPtr<Gtk::Window>(window_xml, "ApplicationManager");
  remove_entry   = getWidgetPtr<Gtk::Entry>(window_xml, "RemoveEntry");

  app_bar        = getWidgetPtr<Gnome::UI::AppBar>(window_xml, "AppBar");

  install_button = getWidgetPtr<Gtk::Button>(window_xml, "InstallButton");
  remove_button  = getWidgetPtr<Gtk::Button>(window_xml, "RemoveButton");
  upgrade_button = getWidgetPtr<Gtk::Button>(window_xml, "UpgradeButton");
  

  SearchPattern    =  getWidgetPtr<Gtk::Entry>(window_xml, "SearchPattern");

  about = getWidgetPtr<Gnome::UI::About>(window_xml, "ApplicationManagerAbout");

  update_status_lbl  = getWidgetPtr<Gtk::Label>(window_xml, "UpdateStatusLabel");
  update_details_lbl = getWidgetPtr<Gtk::Label>(window_xml, "UpdateDetailsLabel");

  /*pixmap = getWidgetPtr<Gtk::Image>(window_xml, "DebianLogo");
    pixmap->set ("/usr/share/pixmaps/application-manager-logo.xpm");*/
  

  status          = new StatusInformation (app_bar);
  package_manager = new AptBackend (status);

  MatchList       =  new SearchTree ();
  box             =  getWidgetPtr<Gtk::VBox>(window_xml, "InstallTargetBox");

  box->add (*MatchList);
  box->show_all();

  UpdateList      =  new SearchTree ();
  box             =  getWidgetPtr<Gtk::VBox>(window_xml, "UpdateTargetBox");

  box->add (*UpdateList);
  box->show_all();

  ConnectSignals();

  Glib::signal_idle().connect(SigC::slot(*this,&ApplicationManager::InitializePackageDatabase));

  upgrade_available = false;

  this->SearchPatternChanged();

  window->show();
}

void ApplicationManager::Quit (void)
{
  Gtk::Main::quit();
}

void ApplicationManager::ConnectSignals (void)
{
  Gtk::Widget *widget;
  Gtk::MenuItem *item;
  Gtk::Button *button;

  /*app->delete_event.connect(slot(*this, &ApplicationManager::quit));*/

  install_button->signal_clicked().connect (slot (*this, &ApplicationManager::InstallButtonClicked));
  remove_button->signal_clicked().connect  (slot (*this, &ApplicationManager::RemoveButtonClicked));
  upgrade_button->signal_clicked().connect (slot (*this, &ApplicationManager::UpgradeButtonClicked));

  SearchPattern->signal_changed().connect (slot (*this, &ApplicationManager::SearchPatternChanged));

}

void ApplicationManager::PopupAboutBox (void)
{
  about->show();
}

void ApplicationManager::SetActionSensitivity (bool sensitivity)
{
  install_button->set_sensitive (sensitivity);
  remove_button->set_sensitive (sensitivity);
  upgrade_button->set_sensitive (sensitivity && upgrade_available);
}

void ApplicationManager::InstallButtonClicked (void)
{
  Operation *op;
  SetActionSensitivity(false);
  op = package_manager->install(SearchPattern->get_text());
  op->RegisterCallback (slot (*this, &ApplicationManager::OperationComplete));
  op->Perform();
}

void ApplicationManager::RemoveButtonClicked (void)
{
  Operation *op;
  SetActionSensitivity(false);
  op = package_manager->remove(remove_entry->get_text());
  op->RegisterCallback (slot (*this, &ApplicationManager::OperationComplete));
  cout << "________________________" << endl;
  op->Perform();
}

void ApplicationManager::UpgradeButtonClicked (void)
{
  Operation *op;
  SetActionSensitivity(false);
  op = package_manager->upgrade ();
  op->RegisterCallback (slot (*this, &ApplicationManager::OperationComplete));
  op->Perform();
}

gint ApplicationManager::quit(GdkEventAny *e)
{
     // Save the window's size
     //app->save_size();

     // Stop gtk/gnome message loop
     Gnome::Main::quit();

}

void ApplicationManager::SetUpdateInformation (PackageList *package_list)
{
  GList *node, *list;
  bool no_changes = true;
  std::stringstream newinstalls;
  std::stringstream removes;
  std::stringstream upgrades;

  UpdateList->PopulateTree (package_list->make_package_info_list(package_list->upgrades));

  if (package_list->upgrades) {
    upgrades << g_list_length (package_list->upgrades) << _(" packages will be updated. ");
    no_changes = false;
  }

  if (package_list->newinstalls) {
    newinstalls << g_list_length (package_list->newinstalls) << _(" new packages will be installed. ");
    no_changes = false;
  }
  if (package_list->removes) {
    removes << g_list_length (package_list->removes) << _(" packages will be removed.");
    no_changes = false;
  }


  if (no_changes) {
    update_status_lbl->set_text (_("Sorry, no updates were found."));
    update_details_lbl->hide();
    upgrade_available = false;
  } else {
    update_status_lbl->set_text (_("Updates to your operating system and applications are available."));
    update_details_lbl->set_text (upgrades.str() + newinstalls.str() + removes.str());
    update_details_lbl->show();
    upgrade_available = true;
  }

}

bool ApplicationManager::InitializePackageDatabase (void)
{
  UpdateOperation *op;
  SetActionSensitivity(false);
  op = package_manager->update();
  op->RegisterCallback (slot (*this, &ApplicationManager::UpdateOperationComplete));
  op->Perform();
  return 0;
}

void ApplicationManager::UpdateOperationComplete (UpdateOperation *operation)
{
  Operation *op;

  cout << "5) UpdateOperationComplete called\n";

  op = package_manager->upgrade();
  SetUpdateInformation (op->GetPackageList());
  delete op;
  
  OperationComplete (operation);
}


void ApplicationManager::OperationComplete (Operation *operation)
{

  cout << "6) Operation Complete, operation was " << get_operation_description (operation->type) << "\n";
  cout << "   Operation number was " << operation->type << "\n";

  SetActionSensitivity (true);

  /*if (status->OperationStatus() == ERROR_NO_PERMISSION) {
    ErrorMessage message ("Unable to open the package database, please make sure you are running as root.");
    quit (NULL);
    }*/
}

void ApplicationManager::SearchPatternChanged (void)
{
  GList *packages;

  cout << "Starting search\n";
  packages = package_manager->search (SearchPattern->get_text ());
  cout << "Done with search\n";

  MatchList->PopulateTree (packages);
}
