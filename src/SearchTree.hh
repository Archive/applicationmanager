class SearchTree;

#ifndef SEARCH_TREE_HH
#define SEARCH_TREE_HH

#include <gtkmm.h>

#include "PackageBackend.hh"

class SearchTree : public Gtk::VBox
{

public:
  SearchTree ();

  void SearchTree::PopulateTree(GList *packages);

private:

  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::TreeView m_TreeView;
  Glib::RefPtr<Gtk::ListStore> m_refListStore;  

  typedef std::vector<pkgCache::Package> type_vecItems;
  type_vecItems m_vecItems;

  struct ModelColumns : public Gtk::TreeModelColumnRecord
  {
    Gtk::TreeModelColumn<Glib::ustring> name;
    Gtk::TreeModelColumn<Glib::ustring> description;

    ModelColumns() { add(name); add(description); }
  };

  const ModelColumns m_columns;

  void add_columns();
  void create_model ();
};

#endif
