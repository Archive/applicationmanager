class UpdateOperation;

#ifndef UPDATE_OPERATION_HH
#define UPDATE_OPERATION_HH

#include "Operation.hh"

class UpdateOperation : public Operation {
public:
  void Perform (void);
  UpdateOperation (PackageBackend *backend, StatusInformation *status);
  PackageList *GetPackageList (void);
  void EmitCompleteSignal (void);
  void RegisterCallback (SigC::Slot1<void,UpdateOperation *> callback);
private:
  SigC::Signal1<void, UpdateOperation *> sig_complete;
};

#endif
