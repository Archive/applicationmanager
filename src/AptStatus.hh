#ifndef APT_STATUS_HH
#define APT_STATUS_HH

#include <apt-pkg/acquire.h>
#include "StatusInformation.hh"

class AptStatus : public pkgAcquireStatus
{
  StatusInformation *status;

public:
   
  virtual bool MediaChange(string Media,string Drive);
  virtual void IMSHit(pkgAcquire::ItemDesc &Itm);
  virtual void Fetch(pkgAcquire::ItemDesc &Itm);
  virtual void Done(pkgAcquire::ItemDesc &Itm);
  virtual void Fail(pkgAcquire::ItemDesc &Itm);
  virtual void Start();
  virtual void Stop();
  
  bool Pulse(pkgAcquire *Owner);
  
  AptStatus(StatusInformation *status);
  
private:
};

#endif
