class AptBackend;

#ifndef APT_BACKEND_HH
#define APT_BACKEND_HH

#include "PackageBackend.hh"
#include "PackageList.hh"
#include "AptCacheFile.hh"
#include "StatusInformation.hh"
#include "Operation.hh"
#include "UpdateOperation.hh"

class AptBackend : virtual public PackageBackend
{
public:
  AptBackend (StatusInformation *status);

  void Print (void);
  Operation *install (string name);
  Operation *remove  (string name);
  Operation *upgrade (void);
  UpdateOperation *update  (void);

  void perform_operation (Operation *operation);
  void perform_update (Operation *operation);

  GList *search (string search_pattern);
private:
  StatusInformation *status;

  AptCacheFile Cache;
  OperationResult InitializeDatabase (void);
  bool InstallPackages(AptCacheFile &Cache);
  OperationResult UpdatePackageList (void);

  bool cache_uninitialized;

  void Lock();
  void Unlock();

  pkgSourceList *SrcList;
};

#endif
