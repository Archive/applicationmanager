#include "PackageList.hh"
#include "PackageInformation.hh"

PackageList::PackageList (AptCacheFile &Cache)
{
  newinstalls = NULL;
  removes = NULL;
  upgrades = NULL;

  unsigned int j;

  this->Cache = &Cache;

  for (j = 0; j < Cache->Head().PackageCount; j++) {
    pkgCache::PkgIterator *I = new pkgCache::PkgIterator(Cache,Cache.List[j]);

    if (Cache[*I].Keep() == true) {
    } else if (Cache[*I].NewInstall() == true) {
      newinstalls = g_list_append (newinstalls, I);
      Cache->MarkKeep(*I);
    } else if (Cache[*I].Delete() == true) {
      removes = g_list_append (removes, I);
      Cache->MarkKeep(*I);
    } else if (Cache[*I].Upgrade() == true) {
      upgrades = g_list_append (upgrades, I);
      Cache->MarkKeep(*I);
    } else {
      delete I;
      g_assert (false);
    }
  }
}

void PackageList::SetupCache (AptCacheFile &Cache)
{
  GList *node;
  pkgCache::PkgIterator *I;

  for (node = newinstalls; node != NULL; node = node->next) {
    I = (pkgCache::PkgIterator *)(node->data);
    printf ("%s", I->Name());
    Cache->MarkInstall (*I);
  }

  for (node = removes; node != NULL; node = node->next) {
    I = (pkgCache::PkgIterator *)(node->data);
    Cache->MarkDelete (*I);
  }

  for (node = upgrades; node != NULL; node = node->next) {
    I = (pkgCache::PkgIterator *)(node->data);
    Cache->MarkInstall (*I);
  }
}

GList *PackageList::make_package_info_list (GList *iterator_list)
{
  GList *node, *pi_list;
  pkgCache::PkgIterator *P;

  pi_list = NULL;

  for (node = iterator_list; node != NULL; node = node->next) {
    P = (pkgCache::PkgIterator *)(node->data);
    PackageInformation *info = new PackageInformation (P->Name(), "Foo"/*P->ShortDesc()*/);
    pi_list = g_list_append (pi_list, info);
  }

  return pi_list;
}
