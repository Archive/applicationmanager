#include "PackageBackend.hh"
#include "Operation.hh"

class ArgumentStorage {
public:
  PackageBackend *backend;
  string name;
  Operation *operation;
};

extern "C" void *spawned_update  (void *);
extern "C" void *spawned_perform_operation (void *);

void *spawned_update (void *data)
{
  //void *blah;

  ArgumentStorage *arguments = (ArgumentStorage *) data;

  arguments->backend->perform_update (arguments->operation);
  //pthread_exit (blah);
}

void *spawned_perform_operation (void *data)
{
  ArgumentStorage *arguments;

  arguments = (ArgumentStorage *) data;
  arguments->backend->perform_operation (arguments->operation);
}

static void
spawn (void *function (void *), PackageBackend *backend, Operation *operation)
{
  pthread_t thread;
  ArgumentStorage *arguments;

  arguments = new ArgumentStorage;

  arguments->backend = backend;
  arguments->operation = operation;
  pthread_create (&thread, 0/*pthread_attr_default*/,
		  function, (void *) arguments);
}

void run_perform_update (PackageBackend *backend, Operation *operation)
{
  spawn (&spawned_update, backend, operation);
}

void run_perform_operation (PackageBackend *backend, Operation *operation)
{
  spawn (&spawned_perform_operation, backend, operation);
}
