#ifndef MESSAGE_BOX_HH
#define MESSAGE_BOX_HH

#include <glibmm.h>

class ErrorMessage {
public:
  ErrorMessage (Glib::ustring message);
};

class InformationMessage {
public:
  InformationMessage (Glib::ustring message);
};

#endif
