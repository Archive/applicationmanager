#include "AptStatus.hh"

#include <apt-pkg/acquire-item.h>
#include <apt-pkg/acquire-worker.h>
#include <apt-pkg/strutl.h>
#include <apt-pkg/error.h>

AptStatus::AptStatus (StatusInformation *status) :
    status(status)
{
}

void AptStatus::Start() 
{
   pkgAcquireStatus::Start(); 
};

// AptStatus::IMSHit - Called when an item got a HIT response	/*{{{*/
// ---------------------------------------------------------------------
/* */
void AptStatus::IMSHit(pkgAcquire::ItemDesc &Itm)
{
  /*
   if (Quiet > 1)
      return;

   if (Quiet <= 0)
      cout << '\r' << BlankLine << '\r';   
   
   cout << _("Hit ") << Itm.Description;
   if (Itm.Owner->FileSize != 0)
      cout << " [" << SizeToStr(Itm.Owner->FileSize) << "B]";
   cout << endl;
   Update = true;
  */
};


void AptStatus::Fetch(pkgAcquire::ItemDesc &Itm)
{
  status->UpdateDescription ("Downloading " + Itm.ShortDesc);
};

void AptStatus::Done(pkgAcquire::ItemDesc &Itm)
{
};

void AptStatus::Fail(pkgAcquire::ItemDesc &Itm)
{
};

void AptStatus::Stop()
{
   pkgAcquireStatus::Stop();
}

bool AptStatus::Pulse(pkgAcquire *Owner)
{
   pkgAcquireStatus::Pulse(Owner);

   status->UpdateCurrent (CurrentBytes, CurrentItems);
   status->UpdateTotal   (TotalBytes, TotalItems);
   status->SetPercent ((double)(CurrentBytes + CurrentItems) / (double)(TotalBytes + TotalItems));

   return true;
}

bool AptStatus::MediaChange(string Media,string Drive)
{
   ioprintf(cout, "Media Change: Please insert the disc labeled '%s' in "
		   "the drive '%s' and press enter\n",
	    Media.c_str(),Drive.c_str());

   char C = 0;
   while (C != '\n' && C != '\r')
      read(STDIN_FILENO,&C,1);
   
   return true;
}

