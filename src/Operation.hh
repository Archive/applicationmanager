class Operation;

#ifndef OPERATION_HH
#define OPERATION_HH

#include "PackageList.hh"
#include "StatusInformation.hh"

// pre-declare to avoid loopiness
class PackageBackend;

typedef enum {
  NO_ERROR,
  ERROR_NO_PERMISSION
} OperationResult;

typedef enum {
  REMOVE_OPERATION,
  INSTALL_OPERATION,
  UPDATE_OPERATION,
  UPGRADE_OPERATION
} OperationType;

class Operation {
public:
  // Do whatever this particular Operation is intended to do (install? remove? upgrade?)
  virtual void Perform (void);

  // Fetch the list describing how this operation will affect packages (installs, removes, upgrades, etc)
  virtual PackageList *GetPackageList (void);

  // What was the result of the operation?
  OperationResult GetResult (void);

  // Callers can register callback functions for notification when the operation completes
  virtual void RegisterCallback (SigC::Slot1<void,Operation *> callback);

  OperationType type;
  
protected:
  PackageList *packages;
  StatusInformation *status;
  PackageBackend *package_backend;
  OperationResult result;

private:
  SigC::Signal1<void, Operation *> sig_complete;

public:
  Operation (OperationType op, PackageBackend *package_backend, StatusInformation *status, PackageList *packages);
  Operation (OperationType op, PackageBackend *package_backend, StatusInformation *status);

  // Called by the backend to specify that a phase has been completed
  virtual void Complete (OperationResult result);

  // Called by the Status object *only on the GTK thread* to inform the operation
  // to emit its completion signal
  virtual void EmitCompleteSignal (void);
};

string get_operation_description (OperationType op);
/*
string get_operation_past_tense (Operation op)
{
  switch (op) {
  UPGRADE_OPERATION:
    return "System is now up to date.";
  UPDATE_OPERATION:
    return "Package list is now up to date.";
  INSTALL_OPERATION:
    return "Installed";
  REMOVE_OPERATION:
    return "Removed";
  }
}
*/

#endif
