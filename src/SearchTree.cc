#include "SearchTree.hh"
#include "PackageInformation.hh"

SearchTree::SearchTree (void)
{
  create_model();

  /* create tree view */
  m_TreeView.set_rules_hint();
  m_TreeView.set_search_column(m_columns.description.index());

  add_columns();  

  m_ScrolledWindow.set_policy (Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
  m_ScrolledWindow.add(m_TreeView);
  add (m_ScrolledWindow);
}

void SearchTree::PopulateTree(GList *packages)
{
  GList *node;

  m_refListStore = Gtk::ListStore::create(m_columns);

  //cout << "Number of packages is " << g_list_length (packages) << "\n";
  //cout << "Creating the model\n";
  for (node = packages; node != NULL; node = node->next) {
    Gtk::TreeRow row = *(m_refListStore->prepend());
    PackageInformation *info;

    info = (PackageInformation *) node->data;

    row[m_columns.name]        = info->Name;
    row[m_columns.description] = info->Description;    
  }
  //cout << "Done with the model\n";

  m_TreeView.set_model(m_refListStore);

}

void SearchTree::add_columns()
{
  /* column for names */
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );

    int cols_count =m_TreeView.insert_column(-1, _("Package"), *pRenderer);
    Gtk::TreeViewColumn* pColumn = m_TreeView.get_column(cols_count-1);
    
    pColumn->set_sizing (Gtk::TREE_VIEW_COLUMN_FIXED);
    pColumn->set_fixed_width (200);

    pColumn->add_attribute(pRenderer->property_text(), m_columns.name);
  }

  /* column for descriptions */
  {
    Gtk::CellRendererText* pRenderer = Gtk::manage( new Gtk::CellRendererText() );

    int cols_count = m_TreeView.insert_column(-1, _("Description"), *pRenderer);
    Gtk::TreeViewColumn* pColumn = m_TreeView.get_column(cols_count-1);

    pColumn->set_sizing (Gtk::TREE_VIEW_COLUMN_FIXED);

    pColumn->add_attribute(pRenderer->property_text(), m_columns.description);
  }
}

void SearchTree::create_model ()
{

}
