class AptCacheFile;

#ifndef APT_CACHE_FILE
#define APT_CACHE_FILE

#include "AptIncludes.hh"
#include "AptOpProgress.hh"
#include "StatusInformation.hh"

class AptCacheFile : public pkgCacheFile
{
  static pkgCache *SortCache;
  static int NameComp(const void *a,const void *b);
  
public:
  void EmptyChanges (void);

  pkgCache::Package **List;
  
  void Sort();
  bool CheckDeps(bool AllowBroken = true);
  bool Open(StatusInformation *status, bool WithLock); 
  AptCacheFile() : List(0) {};
};

#endif
