class ApplicationManager;

#ifndef APPLICATION_MANAGER_HH
#define APPLICATION_MANAGER_HH

#include <glib.h>

#include <libgnomeuimm.h>

#include <glade/glade.h>

#include <gtkmm.h>

#include "PackageBackend.hh"
#include "AptBackend.hh"
#include "StatusInformation.hh"
#include "Operation.hh"
#include "UpdateOperation.hh"
#include "SearchTree.hh"

class ApplicationManager : public SigC::Object
{

public:
  ApplicationManager(const char *glade_filename);

private:
  char *glade_filename;

  Gtk::Window *window;
  GladeXML *window_xml;
  PackageBackend *package_manager;

  StatusInformation *status;
  bool upgrade_available;

  SearchTree *MatchList;
  SearchTree *UpdateList;

  Gtk::Button *install_button;
  Gtk::Button *remove_button;
  Gtk::Button *upgrade_button;
  Gtk::Entry *remove_entry;
  Gtk::Entry *SearchPattern;
  Gnome::UI::About *about;

  Gtk::Label *update_details_lbl;
  Gtk::Label *update_status_lbl;

  gint quit(GdkEventAny *e);

  void Quit (void);

  bool InitializePackageDatabase (void);

  void SetUpdateInformation (PackageList *list);
  void ConnectSignals (void);
  void InstallButtonClicked (void);
  void RemoveButtonClicked (void);
  void UpgradeButtonClicked (void);
  void SetActionSensitivity(bool sensitivity);
  void OperationComplete (Operation *operation);
  void UpdateOperationComplete (UpdateOperation *operation);
  void PopupAboutBox (void);
  void SearchPatternChanged (void);
};

#endif
