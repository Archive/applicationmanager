#ifndef ENSURE_ROOT_H
#define ENSURE_ROOT_H

#include <glib.h>

int ensure_root (char **argv);

#endif
