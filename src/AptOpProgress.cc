#include "AptOpProgress.hh"

#include <string.h>
#include <stdio.h>

void AptOpProgress::Update()
{
  if (CheckChange(0.05) == false)
      return;

  //std::cout << "Hit update with percentage " << Percent << endl;

   char S[300];

   // Print the spinner
   status->UpdateDescription (Op);
   status->SetPercent ((float)Percent / 100.0f);
   status->UpdateTotal   (100, 0);
   //bar->get_progress()->set_percentage (((float)Percent) / 100.0f);
}
