#include "AptCacheFile.hh"

void AptCacheFile::EmptyChanges (void)
{
  unsigned int j;
  for (j = 0; j < (*this)->Head().PackageCount; j++) {
    pkgCache::PkgIterator *I = new pkgCache::PkgIterator((*this), this->List[j]);
    if ((*this)[*I].Keep() == false) {
      this->DCache->MarkKeep(*I);
    }
  }
}

bool 
AptCacheFile::Open(StatusInformation *status, bool WithLock = true)
{
  AptOpProgress Prog(status);
  if (pkgCacheFile::Open(Prog,WithLock) == false)
    return false;
  Sort();
  
  return true;
};

// CacheFile::NameComp - QSort compare by name

pkgCache *AptCacheFile::SortCache = 0;
int AptCacheFile::NameComp(const void *a,const void *b)
{
   if (*(pkgCache::Package **)a == 0 || *(pkgCache::Package **)b == 0)
      return *(pkgCache::Package **)a - *(pkgCache::Package **)b;
   
   const pkgCache::Package &A = **(pkgCache::Package **)a;
   const pkgCache::Package &B = **(pkgCache::Package **)b;

   return strcmp(SortCache->StrP + A.Name,SortCache->StrP + B.Name);
}



// CacheFile::Sort - Sort by name

void AptCacheFile::Sort()
{
   delete [] List;
   List = new pkgCache::Package *[Cache->Head().PackageCount];
   memset(List,0,sizeof(*List)*Cache->Head().PackageCount);
   pkgCache::PkgIterator I = Cache->PkgBegin();
   for (;I.end() != true; I++)
      List[I->ID] = I;

   SortCache = *this;
   qsort(List,Cache->Head().PackageCount,sizeof(*List),NameComp);
}


// CacheFile::CheckDeps - Open the cache file	
// ---------------------------------------------------------------------
/* This routine generates the caches and then opens the dependency cache
   and verifies that the system is OK. */

bool AptCacheFile::CheckDeps(bool AllowBroken)
{
   if (_error->PendingError() == true)
      return false;

   // Check that the system is OK
   if (DCache->DelCount() != 0 || DCache->InstCount() != 0)
      return _error->Error("Internal Error, non-zero counts");
   
   // Apply corrections for half-installed packages
   if (pkgApplyStatus(*DCache) == false)
      return false;
   
   // Nothing is broken
   if (DCache->BrokenCount() == 0 || AllowBroken == true)
      return true;

   // Attempt to fix broken things
   if (_config->FindB("APT::Get::Fix-Broken",false) == true)
   {
      cout << "Correcting dependencies..." << flush;
      if (pkgFixBroken(*DCache) == false || DCache->BrokenCount() != 0)
      {
	 cout << " failed." << endl;
	 //FIXME: ShowBroken(cout,*this,true);

	 cout <<"Unable to correct dependencies";
      }
      if (pkgMinimizeUpgrade(*DCache) == false)
	cout <<"Unable to minimize the upgrade set";
      
      cout << " Done" << endl;
   }
   else
   {
      cout << "You might want to run `apt-get -f install' to correct these." << endl;
      //FIXME: ShowBroken(cout,*this,true);

      cout << "Unmet dependencies. Try using -f.";
   }
      
   return true;
}
