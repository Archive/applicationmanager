#include "StatusInformation.hh"

#include <glib.h>
#include <gtkmm/main.h>

using SigC::bind;
using SigC::slot;

StatusInformation::StatusInformation (Gnome::UI::AppBar *bar)
{
  this->bar = bar;

  pending_call = false;
  connection_mutex = g_mutex_new ();
  total_bytes = 1;
  total_items = 1;
  current_bytes = 0;
  current_items = 0;
  percentage = 0.0f;
  operation_complete = false;
}

void StatusInformation::RegisterForUpdate (void)
{
  g_mutex_lock (connection_mutex);
  if (!pending_call) {
    connection = Glib::signal_idle().connect(slot(*this,&StatusInformation::UpdateInsideGTKLoop));
    pending_call = true;
  }
  g_mutex_unlock (connection_mutex);
}

bool StatusInformation::UpdateInsideGTKLoop (void)
{
  g_mutex_lock (connection_mutex);
  connection.disconnect();
  pending_call = false;

  bar->get_progress()->set_fraction (percentage);
  bar->set_status (status);

  if (operation_complete) {
    operation_complete = false;
    cout << "3) Emitting complete signal\n";
    operation->EmitCompleteSignal ();
    bar->get_progress()->set_fraction (0.0);
  }
  g_mutex_unlock (connection_mutex);
}

void StatusInformation::UpdateDescription (string status)
{
  this->status = status;
  RegisterForUpdate();
}

void StatusInformation::SetPercent (float percentage)
{
  this->percentage = percentage;
}

void StatusInformation::UpdateCurrent (int current_bytes, int current_items)
{
  this->current_bytes = current_bytes;
  this->current_items = current_items;
  RegisterForUpdate();
}

void StatusInformation::UpdateTotal (int total_bytes, int total_items)
{
  this->total_bytes = total_bytes;
  this->total_items = total_items;
  RegisterForUpdate();
}

void StatusInformation::SetObject (string object) 
{
  this->object = object;
}

void StatusInformation::NotifyOperationComplete (Operation *operation)
{
  cout << "2) Notified that the operation was complete\n";
  this->operation = operation;
  this->operation_complete = true;
  status = "";
  RegisterForUpdate();
}
